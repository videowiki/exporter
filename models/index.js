const mongoose = require('mongoose');
const { SchemaNames } = require('./Schemas/utils/schemaNames');

const ArticleSchemas = require('./Schemas/Article');
const OrganizationSchemas = require('./Schemas/Organization');
const UserSchemas = require('./Schemas/User');
const VideoSchemas = require('./Schemas/Video');
const TranslationExportSchemas = require('./Schemas/TranslationExport'); 
const SubtitlesSchemas = require('./Schemas/Subtitles');

const Article = mongoose.model(SchemaNames.article, ArticleSchemas.ArticleSchema);
const Organization = mongoose.model(SchemaNames.organization, OrganizationSchemas.OrganizationSchema);
const User = mongoose.model(SchemaNames.user, UserSchemas.UserSchema);
const Video = mongoose.model(SchemaNames.video, VideoSchemas.VideoSchema);
const TranslationExport = mongoose.model(SchemaNames.translationExport, TranslationExportSchemas.TranslationExport);
const Subtitles = mongoose.model(SchemaNames.subtitles, SubtitlesSchemas.SubtitlesSchema);

module.exports = {
    TranslationExport,
    Organization,
    Subtitles,
    Article,
    Video,
    User,
}