const SchemaNames = {
    commentsThread: 'commentsThread',
    article: 'article',
    organization: 'organization',
    translationRequest: 'translationRequest',
    user: 'user',
    video: 'video',
    translationExport: 'translationExport',
    subtitles: 'subtitles',
}

module.exports = {
    SchemaNames,
}