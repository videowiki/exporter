const SubtitlesModel = require('../models').Subtitles;

function findById(videoId) {
    return SubtitlesModel.findById(videoId)
}

function update(conditions, keyValMap) {
    return SubtitlesModel.updateMany(condition, { $set: keyValMap })
}

function updateById(id, keyValMap) {
    return SubtitlesModel.findByIdAndUpdate(id, { $set: keyValMap })
}

function find(conditions) {
    return SubtitlesModel.find(conditions)
}

function create(values) {
    return SubtitlesModel.create(values);
}

module.exports = {
    findById,
    update,
    updateById,
    find,
    create,
}
