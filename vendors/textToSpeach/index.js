const { textToSpeech, GOOGLE_VOICES_IDS, AWS_VOICES_IDS } = require('./TextToSpeechUtils');


module.exports = {
    convertTextToSpeech: textToSpeech,
    GOOGLE_VOICES_IDS,
    AWS_VOICES_IDS,
}